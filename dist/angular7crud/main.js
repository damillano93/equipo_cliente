(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/Equipos/add-equipos/equipos-add.component.css":
/*!***************************************************************!*\
  !*** ./src/app/Equipos/add-equipos/equipos-add.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL0VxdWlwb3MvYWRkLWVxdWlwb3MvZXF1aXBvcy1hZGQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/Equipos/add-equipos/equipos-add.component.html":
/*!****************************************************************!*\
  !*** ./src/app/Equipos/add-equipos/equipos-add.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<h1> <i class=\"far fa-plus-square\"></i> Crear Equipos</h1>\n<div class=\"card\">\n  <div class=\"card-body\">\n    <form [formGroup]=\"angForm\" novalidate>\n\n      <div class=\"form-group\">\n        <label class=\"col-md-4\">Placa_Inventario</label>\n        <input type=\"text\" class=\"form-control\" formControlName=\"Placa_Inventario\" #Placa_Inventario/>\n      </div>\n      <div *ngIf=\"angForm.controls['Placa_Inventario'].invalid && (angForm.controls['Placa_Inventario'].dirty || angForm.controls['Placa_Inventario'].touched)\" class=\"alert alert-danger\">\n        <div *ngIf=\"angForm.controls['Placa_Inventario'].errors.required\">\n           dos Placa_Inventario es requerido.\n        </div>\n      </div>\n\n      <div class=\"form-group\">\n        <label class=\"col-md-4\">Nombre</label>\n        <input type=\"text\" class=\"form-control\" formControlName=\"Nombre\" #Nombre/>\n      </div>\n      <div *ngIf=\"angForm.controls['Nombre'].invalid && (angForm.controls['Nombre'].dirty || angForm.controls['Nombre'].touched)\" class=\"alert alert-danger\">\n        <div *ngIf=\"angForm.controls['Nombre'].errors.required\">\n          Nombre es requerido.\n        </div>\n      </div>\n\n      <div class=\"form-group\">\n        <label class=\"col-md-4\">Descripcion</label>\n        <input type=\"text\" class=\"form-control\" formControlName=\"Descripcion\" #Descripcion/>\n      </div>\n      <div *ngIf=\"angForm.controls['Descripcion'].invalid && (angForm.controls['Descripcion'].dirty || angForm.controls['Descripcion'].touched)\" class=\"alert alert-danger\">\n        <div *ngIf=\"angForm.controls['Descripcion'].errors.required\">\n          Descripcion es requerido.\n        </div>\n      </div>\n\n      <div class=\"form-group\">\n        <label class=\"col-md-4\">Ubicacion</label>\n        <input type=\"text\" class=\"form-control\" formControlName=\"Ubicacion\" #Ubicacion/>\n      </div>\n      <div *ngIf=\"angForm.controls['Ubicacion'].invalid && (angForm.controls['Ubicacion'].dirty || angForm.controls['Ubicacion'].touched)\" class=\"alert alert-danger\">\n        <div *ngIf=\"angForm.controls['Ubicacion'].errors.required\">\n          Ubicacion es requerido.\n        </div>\n      </div>\n\n      <div class=\"form-group\">\n        <div class=\"row\">\n          <div class=\"col-5\">\n            <label class=\"col-md-8\">Codigo_QR</label>\n          \n            <input type=\"text\" class=\"form-control\" formControlName=\"Codigo_QR\" #Codigo_QR/>\n         \n          </div>\n     \n\n        </div>\n      </div>\n      <div *ngIf=\"angForm.controls['Codigo_QR'].invalid && (angForm.controls['Codigo_QR'].dirty || angForm.controls['Codigo_QR'].touched)\" class=\"alert alert-danger\">\n        <div *ngIf=\"angForm.controls['Codigo_QR'].errors.required\">\n          Codigo_QR es requerido.\n        </div>\n      </div>\n\n            \n      <div class=\"form-group\">\n        <button (click)=\"addEquipos(Placa_Inventario.value, Nombre.value, Descripcion.value, Codigo_QR.value, Ubicacion.value)\"\n          [disabled]=\"angForm.pristine || angForm.invalid\"\n          class=\"btn btn-primary\">Crear Equipos</button>\n      </div>\n\n     \n    </form>\n  </div>\n</div>\n\n\n\n\n  \n\n\n"

/***/ }),

/***/ "./src/app/Equipos/add-equipos/equipos-add.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/Equipos/add-equipos/equipos-add.component.ts ***!
  \**************************************************************/
/*! exports provided: EquiposAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EquiposAddComponent", function() { return EquiposAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_equipos_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/equipos.service */ "./src/app/services/equipos.service.ts");
/* harmony import */ var ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng6-toastr-notifications */ "./node_modules/ng6-toastr-notifications/fesm5/ng6-toastr-notifications.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EquiposAddComponent = /** @class */ (function () {
    function EquiposAddComponent(fb, Equipos_ser, toastr) {
        this.fb = fb;
        this.Equipos_ser = Equipos_ser;
        this.toastr = toastr;
        this.Codigo_QR = {};
        this.createForm();
    }
    EquiposAddComponent.prototype.createForm = function () {
        this.angForm = this.fb.group({
            Placa_Inventario: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            Nombre: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            Descripcion: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            Codigo_QR: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            Ubicacion: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
    };
    EquiposAddComponent.prototype.showSuccess = function () {
        this.toastr.successToastr('El registro fue creado.', 'Creado!');
    };
    EquiposAddComponent.prototype.showError = function () {
        this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
    };
    EquiposAddComponent.prototype.addEquipos = function (Placa_Inventario, Nombre, Descripcion, Codigo_QR, Ubicacion) {
        this.Equipos_ser.addEquipos(Placa_Inventario, Nombre, Descripcion, Codigo_QR, Ubicacion);
        this.angForm.reset();
        this.showSuccess();
    };
    EquiposAddComponent.prototype.ngOnInit = function () {
    };
    EquiposAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-equipos-add',
            template: __webpack_require__(/*! ./equipos-add.component.html */ "./src/app/Equipos/add-equipos/equipos-add.component.html"),
            styles: [__webpack_require__(/*! ./equipos-add.component.css */ "./src/app/Equipos/add-equipos/equipos-add.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _services_equipos_service__WEBPACK_IMPORTED_MODULE_2__["EquiposService"],
            ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_3__["ToastrManager"]])
    ], EquiposAddComponent);
    return EquiposAddComponent;
}());



/***/ }),

/***/ "./src/app/Equipos/edit-equipos/equipos-edit.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/Equipos/edit-equipos/equipos-edit.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL0VxdWlwb3MvZWRpdC1lcXVpcG9zL2VxdWlwb3MtZWRpdC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/Equipos/edit-equipos/equipos-edit.component.html":
/*!******************************************************************!*\
  !*** ./src/app/Equipos/edit-equipos/equipos-edit.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1> <i class=\"far fa-edit\"></i> Editar Equipos</h1>\n<div class=\"card\">\n  <div class=\"card-body\">\n    <form [formGroup]=\"angForm\" novalidate>\n\n        <div class=\"form-group\">\n          <label class=\"col-md-4\">Placa_Inventario</label>\n          <input type=\"text\" class=\"form-control\" formControlName=\"Placa_Inventario\" #Placa_Inventario [(ngModel)] = \"equipos.Placa_Inventario\"/>\n        </div>\n        <div *ngIf=\"angForm.controls['Placa_Inventario'].invalid && (angForm.controls['Placa_Inventario'].dirty || angForm.controls['Placa_Inventario'].touched)\" class=\"alert alert-danger\">\n          <div *ngIf=\"angForm.controls['Placa_Inventario'].errors.required\">\n            Placa_Inventario es requerido.\n          </div>\n        </div>\n\n        <div class=\"form-group\">\n          <label class=\"col-md-4\">Nombre</label>\n          <input type=\"text\" class=\"form-control\" formControlName=\"Nombre\" #Nombre [(ngModel)] = \"equipos.Nombre\"/>\n        </div>\n        <div *ngIf=\"angForm.controls['Nombre'].invalid && (angForm.controls['Nombre'].dirty || angForm.controls['Nombre'].touched)\" class=\"alert alert-danger\">\n          <div *ngIf=\"angForm.controls['Nombre'].errors.required\">\n            Nombre es requerido.\n          </div>\n        </div>\n\n        <div class=\"form-group\">\n          <label class=\"col-md-4\">Descripcion</label>\n          <input type=\"text\" class=\"form-control\" formControlName=\"Descripcion\" #Descripcion [(ngModel)] = \"equipos.Descripcion\"/>\n        </div>\n        <div *ngIf=\"angForm.controls['Descripcion'].invalid && (angForm.controls['Descripcion'].dirty || angForm.controls['Descripcion'].touched)\" class=\"alert alert-danger\">\n          <div *ngIf=\"angForm.controls['Descripcion'].errors.required\">\n            Descripcion es requerido.\n          </div>\n        </div>\n\n        <div class=\"form-group\">     \n          <label class=\"col-md-4\">Ubicacion</label>\n          <input type=\"text\" class=\"form-control\" formControlName=\"Ubicacion\" #Ubicacion [(ngModel)] = \"equipos.Ubicacion\"/>\n        </div>\n        <div *ngIf=\"angForm.controls['Ubicacion'].invalid && (angForm.controls['Ubicacion'].dirty || angForm.controls['Ubicacion'].touched)\" class=\"alert alert-danger\">\n            <div *ngIf=\"angForm.controls['Ubicacion'].errors.required\">\n              Ubicacion es requerido.\n            </div>\n        </div>\n\n        <div class=\"form-group\">\n          <div class=\"row\">             \n            <div class=\"col-5\">\n              <label class=\"col-md-4\">Codigo_QR</label>\n              <input type=\"text\" class=\"form-control\" formControlName=\"Codigo_QR\" #Codigo_QR [(ngModel)] = \"equipos.Codigo_QR\" disabled/>\n            </div>\n            <div class=\"col\">\n              <qr-code [value]=\"equipos.Codigo_QR\" [size]=\"120\"></qr-code>\n             \n            </div>    \n          </div>  \n          \n        </div>  \n        <div *ngIf=\"angForm.controls['Codigo_QR'].invalid && (angForm.controls['Codigo_QR'].dirty || angForm.controls['Codigo_QR'].touched)\" class=\"alert alert-danger\">\n          <div *ngIf=\"angForm.controls['Codigo_QR'].errors.required\">\n            Codigo_QR es requerido.\n          </div>\n        </div>\n\n        \n\n        <div class=\"form-group\">\n          <button (click)=\"updateEquipos(Placa_Inventario.value, Nombre.value, Descripcion.value, Codigo_QR.value, Ubicacion.value)\"\n            [disabled]=\"angForm.pristine || angForm.invalid\"\n            class=\"btn btn-primary\"> Equipos</button>\n        </div>\n\n    </form>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/Equipos/edit-equipos/equipos-edit.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/Equipos/edit-equipos/equipos-edit.component.ts ***!
  \****************************************************************/
/*! exports provided: EquiposEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EquiposEditComponent", function() { return EquiposEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_equipos_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/equipos.service */ "./src/app/services/equipos.service.ts");
/* harmony import */ var ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng6-toastr-notifications */ "./node_modules/ng6-toastr-notifications/fesm5/ng6-toastr-notifications.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EquiposEditComponent = /** @class */ (function () {
    function EquiposEditComponent(route, router, toastr, bs, fb) {
        this.route = route;
        this.router = router;
        this.toastr = toastr;
        this.bs = bs;
        this.fb = fb;
        this.equipos = {};
        this.createForm();
    }
    EquiposEditComponent.prototype.createForm = function () {
        this.angForm = this.fb.group({
            Placa_Inventario: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            Nombre: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            Descripcion: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            Codigo_QR: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            Ubicacion: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    };
    EquiposEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.bs.editEquipos(params['id']).subscribe(function (res) {
                _this.equipos = res;
            });
        });
    };
    EquiposEditComponent.prototype.showInfo = function () {
        this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
    };
    EquiposEditComponent.prototype.updateEquipos = function (Placa_Inventario, Nombre, Descripcion, Codigo_QR, Ubicacion) {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.bs.updateEquipos(Placa_Inventario, Nombre, Descripcion, Codigo_QR, Ubicacion, params['id']);
            _this.showInfo();
            _this.router.navigate(['equipos']);
        });
    };
    EquiposEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-equipos-edit',
            template: __webpack_require__(/*! ./equipos-edit.component.html */ "./src/app/Equipos/edit-equipos/equipos-edit.component.html"),
            styles: [__webpack_require__(/*! ./equipos-edit.component.css */ "./src/app/Equipos/edit-equipos/equipos-edit.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_4__["ToastrManager"],
            _services_equipos_service__WEBPACK_IMPORTED_MODULE_3__["EquiposService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
    ], EquiposEditComponent);
    return EquiposEditComponent;
}());



/***/ }),

/***/ "./src/app/Equipos/get-equipos/equipos-get.component.css":
/*!***************************************************************!*\
  !*** ./src/app/Equipos/get-equipos/equipos-get.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*\n   server-side-angular-way.component.css\n*/\n.no-data-available {\n    text-align: center;\n  }\n/*\n     src/styles.css (i.e. your global style)\n  */\n.dataTables_empty {\n    display: none;\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvRXF1aXBvcy9nZXQtZXF1aXBvcy9lcXVpcG9zLWdldC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztFQUVFO0FBQ0Y7SUFDSSxtQkFBbUI7R0FDcEI7QUFFRDs7SUFFRTtBQUNGO0lBQ0UsY0FBYztHQUNmIiwiZmlsZSI6InNyYy9hcHAvRXF1aXBvcy9nZXQtZXF1aXBvcy9lcXVpcG9zLWdldC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLypcbiAgIHNlcnZlci1zaWRlLWFuZ3VsYXItd2F5LmNvbXBvbmVudC5jc3NcbiovXG4ubm8tZGF0YS1hdmFpbGFibGUge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuICBcbiAgLypcbiAgICAgc3JjL3N0eWxlcy5jc3MgKGkuZS4geW91ciBnbG9iYWwgc3R5bGUpXG4gICovXG4gIC5kYXRhVGFibGVzX2VtcHR5IHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9Il19 */"

/***/ }),

/***/ "./src/app/Equipos/get-equipos/equipos-get.component.html":
/*!****************************************************************!*\
  !*** ./src/app/Equipos/get-equipos/equipos-get.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1> <i class=\"far fa-eye\"></i> Ver Equipos</h1>\n<table datatable class=\"table table-striped table-responsive-md btn-table\">\n  <thead>\n  <tr>\n     <td>Placa_Inventario</td>\n <td>Nombre</td>\n <td>Descripcion</td>\n <td>Codigo_QR</td>\n <td>Ubicacion</td>\n\n    <td >Acciones</td>\n  </tr>\n  </thead>\n\n  <tbody>\n     \n           <tr *ngFor=\"let equipos of equipos\">\n<td>{{ equipos.Placa_Inventario}}</td>\n<td>{{ equipos.Nombre}}</td>\n<td>{{ equipos.Descripcion}}</td>\n<td>{{ equipos.Codigo_QR}}</td>\n<td>{{ equipos.Ubicacion}}</td>\n<td><a [routerLink]=\"['edit', equipos._id]\" class=\"btn btn-primary\"><i class=\"far fa-edit\"></i></a>&nbsp;&nbsp;<a (click) = \"deleteEquipos(equipos._id)\" class=\"btn btn-danger\"><i class=\"far fa-trash-alt\"></i></a></td>\n         \n      </tr>\n  </tbody>\n</table>\n"

/***/ }),

/***/ "./src/app/Equipos/get-equipos/equipos-get.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/Equipos/get-equipos/equipos-get.component.ts ***!
  \**************************************************************/
/*! exports provided: EquiposGetComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EquiposGetComponent", function() { return EquiposGetComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_equipos_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/equipos.service */ "./src/app/services/equipos.service.ts");
/* harmony import */ var ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng6-toastr-notifications */ "./node_modules/ng6-toastr-notifications/fesm5/ng6-toastr-notifications.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EquiposGetComponent = /** @class */ (function () {
    function EquiposGetComponent(bs, toastr) {
        this.bs = bs;
        this.toastr = toastr;
    }
    EquiposGetComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.bs
            .getEquipos()
            .subscribe(function (data) {
            _this.equipos = data;
        });
    };
    EquiposGetComponent.prototype.deleteEquipos = function (id) {
        var _this = this;
        this.bs.deleteEquipos(id).subscribe(function (res) {
            console.log('Deleted');
            _this.bs
                .getEquipos()
                .subscribe(function (data) {
                _this.equipos = data;
            });
            _this.showDeleted();
        });
    };
    EquiposGetComponent.prototype.showSuccess = function () {
        this.toastr.successToastr('This is success toast.', 'Success!');
    };
    EquiposGetComponent.prototype.showDeleted = function () {
        this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
    };
    EquiposGetComponent.prototype.showWarning = function () {
        this.toastr.warningToastr('This is warning toast.', 'Alert!');
    };
    EquiposGetComponent.prototype.showInfo = function () {
        this.toastr.infoToastr('This is info toast.', 'Info');
    };
    EquiposGetComponent.prototype.showToast = function (position) {
        if (position === void 0) { position = 'top-left'; }
        this.toastr.infoToastr('This is a toast.', 'Toast', {
            position: position
        });
    };
    EquiposGetComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-equipos-get',
            template: __webpack_require__(/*! ./equipos-get.component.html */ "./src/app/Equipos/get-equipos/equipos-get.component.html"),
            styles: [__webpack_require__(/*! ./equipos-get.component.css */ "./src/app/Equipos/get-equipos/equipos-get.component.css")]
        }),
        __metadata("design:paramtypes", [_services_equipos_service__WEBPACK_IMPORTED_MODULE_1__["EquiposService"], ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_2__["ToastrManager"]])
    ], EquiposGetComponent);
    return EquiposGetComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _Equipos_add_equipos_equipos_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Equipos/add-equipos/equipos-add.component */ "./src/app/Equipos/add-equipos/equipos-add.component.ts");
/* harmony import */ var _Equipos_edit_equipos_equipos_edit_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Equipos/edit-equipos/equipos-edit.component */ "./src/app/Equipos/edit-equipos/equipos-edit.component.ts");
/* harmony import */ var _Equipos_get_equipos_equipos_get_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Equipos/get-equipos/equipos-get.component */ "./src/app/Equipos/get-equipos/equipos-get.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _home_home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"]
    },
    {
        path: 'equipos/create',
        component: _Equipos_add_equipos_equipos_add_component__WEBPACK_IMPORTED_MODULE_3__["EquiposAddComponent"]
    },
    {
        path: 'equipos/edit/:id',
        component: _Equipos_edit_equipos_equipos_edit_component__WEBPACK_IMPORTED_MODULE_4__["EquiposEditComponent"]
    },
    {
        path: 'equipos',
        component: _Equipos_get_equipos_equipos_get_component__WEBPACK_IMPORTED_MODULE_5__["EquiposGetComponent"]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h1{\n    text-align:center;\n}\nh2{\n    text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxrQkFBa0I7Q0FDckI7QUFDRDtJQUNJLG1CQUFtQjtDQUN0QiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaDF7XG4gICAgdGV4dC1hbGlnbjpjZW50ZXI7XG59XG5oMntcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59Il19 */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.8.2/css/all.css\"\n    integrity=\"sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay\" crossorigin=\"anonymous\">\n<ng2-slim-loading-bar color=\"blue\"></ng2-slim-loading-bar>\n\n<body>\n\n</body>\n\n\n<nav class=\"navbar navbar-expand-sm bg-dark\">\n  <a  sidebarjsToggle class=\"btn btn-light\"><img width=\"30\" height=\"30\"src=\"assets/img/lineas.png\"></a>\n  <h3 style=\"color:white;\">  &nbsp; &nbsp; Equipos</h3>\n \n</nav>\n\n\n\n<sidebarjs-element (changeVisibility)=\"onEvent($event)\" (open)=\"onEvent($event)\" (close)=\"onEvent($event)\">\n\n  <a class=\"nav-link\" href=\"/\">Equipos</a>\n\n\n   <div class=\"d-inline-block\" ngbDropdown #myDropequipos=\"ngbDropdown\">\n<button class=\"btn btn-outline-primary btn-lg btn-block\" id=\"dropdownManual2\" ngbDropdownAnchor\n(focus)=\"myDropequipos.open()\">Equipos</button>\n<div ngbDropdownMenu aria-labelledby=\"dropdownManual\">\n<a routerLink=\"equipos/create\" class=\"nav-link\" routerLinkActive=\"active\" sidebarjsToggle>\nCrear Equipos\n</a>\n<a routerLink=\"equipos\" class=\"nav-link\" routerLinkActive=\"active\" sidebarjsToggle>\nVer Equipos\n</a>\n</div>\n</div>\n\n\n\n  \n    <p></p>\n\n  <a><button class=\"btn btn-danger\" sidebarjsToggle><img width=\"30\" height=\"30\"src=\"assets/img/close.png\"></button></a>\n</sidebarjs-element>\n\n\n\n<br />\n<div class=\"container\">\n  <router-outlet></router-outlet>\n</div>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_slim_loading_bar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-slim-loading-bar */ "./node_modules/ng2-slim-loading-bar/index.js");
/* harmony import */ var ng_sidebarjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng-sidebarjs */ "./node_modules/ng-sidebarjs/fesm5/ng-sidebarjs.js");
/* harmony import */ var _services_equipos_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services/equipos.service */ "./src/app/services/equipos.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AppComponent = /** @class */ (function () {
    function AppComponent(sidebarjsService, _loadingBar, _router, EquiposSer) {
        var _this = this;
        this.sidebarjsService = sidebarjsService;
        this._loadingBar = _loadingBar;
        this._router = _router;
        this.EquiposSer = EquiposSer;
        this.title = 'equipos';
        this._router.events.subscribe(function (event) {
            _this.navigationInterceptor(event);
        });
    }
    AppComponent.prototype.ngOnInit = function () { };
    AppComponent.prototype.navigationInterceptor = function (event) {
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_4__["NavigationStart"]) {
            this._loadingBar.start();
        }
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_4__["NavigationEnd"]) {
            this._loadingBar.complete();
        }
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_4__["NavigationCancel"]) {
            this._loadingBar.stop();
        }
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_4__["NavigationError"]) {
            this._loadingBar.stop();
        }
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [ng_sidebarjs__WEBPACK_IMPORTED_MODULE_2__["SidebarjsService"],
            ng2_slim_loading_bar__WEBPACK_IMPORTED_MODULE_1__["SlimLoadingBarService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _services_equipos_service__WEBPACK_IMPORTED_MODULE_3__["EquiposService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ng2_slim_loading_bar__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng2-slim-loading-bar */ "./node_modules/ng2-slim-loading-bar/index.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _Equipos_add_equipos_equipos_add_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./Equipos/add-equipos/equipos-add.component */ "./src/app/Equipos/add-equipos/equipos-add.component.ts");
/* harmony import */ var _Equipos_edit_equipos_equipos_edit_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./Equipos/edit-equipos/equipos-edit.component */ "./src/app/Equipos/edit-equipos/equipos-edit.component.ts");
/* harmony import */ var _Equipos_get_equipos_equipos_get_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./Equipos/get-equipos/equipos-get.component */ "./src/app/Equipos/get-equipos/equipos-get.component.ts");
/* harmony import */ var ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ng6-toastr-notifications */ "./node_modules/ng6-toastr-notifications/fesm5/ng6-toastr-notifications.js");
/* harmony import */ var ng_sidebarjs__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ng-sidebarjs */ "./node_modules/ng-sidebarjs/fesm5/ng-sidebarjs.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _services_equipos_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./services/equipos.service */ "./src/app/services/equipos.service.ts");
/* harmony import */ var angular2_qrcode__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! angular2-qrcode */ "./node_modules/angular2-qrcode/lib/angular2-qrcode.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _home_home_component__WEBPACK_IMPORTED_MODULE_9__["HomeComponent"],
                _app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"],
                _Equipos_add_equipos_equipos_add_component__WEBPACK_IMPORTED_MODULE_10__["EquiposAddComponent"],
                _Equipos_get_equipos_equipos_get_component__WEBPACK_IMPORTED_MODULE_12__["EquiposGetComponent"],
                _Equipos_edit_equipos_equipos_edit_component__WEBPACK_IMPORTED_MODULE_11__["EquiposEditComponent"]
            ],
            imports: [
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_15__["NgbModule"],
                ng_sidebarjs__WEBPACK_IMPORTED_MODULE_14__["SidebarjsModule"].forRoot(),
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_1__["BrowserAnimationsModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                ng2_slim_loading_bar__WEBPACK_IMPORTED_MODULE_7__["SlimLoadingBarModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
                angular_datatables__WEBPACK_IMPORTED_MODULE_3__["DataTablesModule"],
                ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_13__["ToastrModule"].forRoot(),
                angular2_qrcode__WEBPACK_IMPORTED_MODULE_17__["QRCodeModule"]
            ],
            providers: [
                _services_equipos_service__WEBPACK_IMPORTED_MODULE_16__["EquiposService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/home/home.component.css":
/*!*****************************************!*\
  !*** ./src/app/home/home.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/home/home.component.html":
/*!******************************************!*\
  !*** ./src/app/home/home.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.8.2/css/all.css\"\n    integrity=\"sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay\" crossorigin=\"anonymous\">\n<h1>Bienvenido</h1>\n<p>&nbsp;</p>\n\n<a routerLink=\"equipos/create\" routerLinkActive=\"active\" class=\"btn btn-primary\">\n<i class=\"far fa-plus-square fa-7x\"></i>\n<h5 class=\"card-title\">Crear Equipos</h5>\n</a>\n&nbsp;\n<a routerLink=\"equipos\" routerLinkActive=\"active\" class=\"btn btn-success\">\n<i class=\"far fa-eye fa-7x\"></i>\n<h5 class=\"card-title\">Ver Equipos</h5>\n</a>\n<p></p>\n\n"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/services/equipos.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/equipos.service.ts ***!
  \*********************************************/
/*! exports provided: EquiposService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EquiposService", function() { return EquiposService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EquiposService = /** @class */ (function () {
    function EquiposService(http) {
        this.http = http;
        this.uri = 'http://54.237.133.97:3002/equipos';
    }
    EquiposService.prototype.addEquipos = function (Placa_Inventario, Nombre, Descripcion, Codigo_QR, Ubicacion) {
        var obj = {
            Placa_Inventario: Placa_Inventario,
            Nombre: Nombre,
            Descripcion: Descripcion,
            Codigo_QR: Codigo_QR,
            Ubicacion: Ubicacion
        };
        this.http.post("" + this.uri, obj)
            .subscribe(function (res) { return console.log('Done'); });
    };
    EquiposService.prototype.getEquipos = function () {
        return this
            .http
            .get("" + this.uri);
    };
    EquiposService.prototype.editEquipos = function (id) {
        return this
            .http
            .get(this.uri + "/" + id);
    };
    EquiposService.prototype.updateEquipos = function (Placa_Inventario, Nombre, Descripcion, Codigo_QR, Ubicacion, id) {
        var obj = {
            Placa_Inventario: Placa_Inventario,
            Nombre: Nombre,
            Descripcion: Descripcion,
            Codigo_QR: Codigo_QR,
            Ubicacion: Ubicacion
        };
        this
            .http
            .put(this.uri + "/" + id, obj)
            .subscribe(function (res) { return console.log('Done'); });
    };
    EquiposService.prototype.deleteEquipos = function (id) {
        return this
            .http
            .delete(this.uri + "/" + id);
    };
    EquiposService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], EquiposService);
    return EquiposService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/david/Documents/equipo_cliente/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map