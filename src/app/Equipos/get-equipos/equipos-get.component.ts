import { Component, OnInit } from '@angular/core';
import Equipos from '../../models/Equipos';
import { EquiposService } from '../../services/equipos.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-equipos-get',
  templateUrl: './equipos-get.component.html',
  styleUrls: ['./equipos-get.component.css']
})

export class EquiposGetComponent implements OnInit {

  equipos: Equipos[];

  constructor( private bs: EquiposService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getEquipos()
      .subscribe((data: Equipos[]) => {
        this.equipos = data;
    });
  }

  deleteEquipos(id) {
    this.bs.deleteEquipos(id).subscribe(res => {
      console.log('Deleted');
      this.bs
      .getEquipos()
      .subscribe((data: Equipos[]) => {
        this.equipos = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

