import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { EquiposService } from '../../services/equipos.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-equipos-edit',
  templateUrl: './equipos-edit.component.html',
  styleUrls: ['./equipos-edit.component.css']
})
export class EquiposEditComponent implements OnInit {
     angForm: FormGroup;
  equipos: any = {};

  constructor(private route: ActivatedRoute,
    private router: Router,
    public toastr: ToastrManager,
    private bs: EquiposService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        Placa_Inventario: ['', Validators.required ],
Nombre: ['', Validators.required ],
Descripcion: ['', Validators.required ],
Codigo_QR: ['', Validators.required ],
Ubicacion: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editEquipos(params['id']).subscribe(res => {
        this.equipos = res;
      });
    });
        }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateEquipos(Placa_Inventario, Nombre, Descripcion, Codigo_QR, Ubicacion ) {
   this.route.params.subscribe(params => {
      this.bs.updateEquipos(Placa_Inventario, Nombre, Descripcion, Codigo_QR, Ubicacion  , params['id']);
      this.showInfo();
      this.router.navigate(['equipos']);
   });
}
}
