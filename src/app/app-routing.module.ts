import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { EquiposAddComponent } from './Equipos/add-equipos/equipos-add.component';
import { EquiposEditComponent } from './Equipos/edit-equipos/equipos-edit.component';
import { EquiposGetComponent } from './Equipos/get-equipos/equipos-get.component';

const routes: Routes = [
  {
  path: '',
  component: HomeComponent
},
  {
  path: 'equipos/create',
  component: EquiposAddComponent
},
{
  path: 'equipos/edit/:id',
  component: EquiposEditComponent
},
{
  path: 'equipos',
  component: EquiposGetComponent
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
