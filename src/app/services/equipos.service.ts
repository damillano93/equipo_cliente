import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EquiposService {

  uri = 'http://54.237.133.97:3002/equipos';

  constructor(private http: HttpClient) { }
  addEquipos(Placa_Inventario , Nombre , Descripcion , Codigo_QR , Ubicacion ) {
    const obj = {
      Placa_Inventario: Placa_Inventario,
Nombre: Nombre,
Descripcion: Descripcion,
Codigo_QR: Codigo_QR,
Ubicacion: Ubicacion

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getEquipos() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editEquipos(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateEquipos(Placa_Inventario , Nombre , Descripcion , Codigo_QR , Ubicacion , id) {

    const obj = {
      Placa_Inventario: Placa_Inventario,
Nombre: Nombre,
Descripcion: Descripcion,
Codigo_QR: Codigo_QR,
Ubicacion: Ubicacion

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteEquipos(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
